#include <stdio.h>
int main()
{
    char str[MAX_SIZE];
    char * s = str;
    printf("Enter your text : ");
    gets(str);

    while(*s) 
    {
        *s = (*s > 'a' && *s <= 'z') ? *s-32 : *s;
        s++;
    }

    printf("Uppercase string : %s",str);
    return 0;
}